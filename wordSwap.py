import sys


def main():
    if len(sys.argv) != 1:
        sys.argv.remove("wordSwap.py")
        swapped_string = swap_words(sys.argv)
    else:
        swapped_string = std_input()
    print(swapped_string)


def std_input():
    """Gets input to swap"""
    print("Please enter a sentence")
    sentence = input()
    sentence = swap_words(sentence.split())
    return sentence


def swap_words(input):
    """Swaps words based on their index using a %"""
    string = ""
    for x, word in enumerate(input):
        if x % 2 == 0:
            string += word+" "
        else:
            string += "Biss10 "
    return string



if __name__ == '__main__':
    main()
